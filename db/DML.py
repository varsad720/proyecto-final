from db.connector import *
from models.alumnos import Alumno, Notas


class DML():

    def calificaralumno(self):
        print("Calificación de Alumnos\n\n")

        #Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)

        nombreA = input("Nombre del Alumno: ")
        apellidoA = input("Apellido del Alumno: ")
        nivelA = input("Año del alumno(1-12): ")
        salonA = input("Salon del alumno(A-Z): ")


        print ("\n\nAsignando notas a", nombreA, apellidoA,".\n")
        nota1A = float(input("Primer trimestre: "))
        nota2A= float(input("Segundo trimestre: "))
        nota3A = float(input("Tercer trimestre: "))
        suma = nota1A + nota2A + nota3A
        promedioA = suma / 3


        alumno1 = Alumno(nombre=nombreA, apellido=apellidoA, nivel=nivelA,
                           salon=salonA)
        session.add(alumno1)
        session.commit()

        if promedioA > 3.0:


            notas = Notas(nota1=nota1A, nota2=nota2A, nota3=nota3A, promedio = promedioA, estado= 'APROBADO' )
            session.add(notas)
            session.commit()

        else:
            notas = Notas(nota1=nota1A, nota2=nota2A, nota3=nota3A, promedio=promedioA, estado= 'REPROBADO')
            session.add(notas)
            session.commit()




    def editaralumno(self):

        nid = int(input("Introduzca el número de ID del Alumno que desea editar: "))
        print("\n===DATOS NUEVOS===\n")
        nombre = str(input("Nombre: "))
        apellido = str(input("Apellido: "))
        nivel = int(input("Grado: "))
        salon = str(input("Salón: "))
        data = session.query(Alumno).filter(Alumno.id == nid).first()



        data.nombre = nombre
        data.apellido = apellido
        data.nivel = nivel
        data.salon = salon

        session.add(data)
        session.commit()


    def listaralumnos(self):

        data = session.query(Alumno).all()

        print("ID||", "NOMBRE ||", "APELLIDO ||", "SALÓN ")
        for alumno in data:

            print(alumno.id,"||", alumno.nombre,"||", alumno.apellido,"||", alumno.nivel,"-", alumno.salon)
        print("")

    def listarnotas(self):
        data = session.query(Notas).all()
        print("ID|", "NOTA1 |", "NOTA2 |", "NOTA3 |", "PROMEDIO")
        for notas in data:

            print(notas.id,"||", notas.nota1,"||", notas.nota2,"||", notas.nota3,"||", notas.promedio,"||", notas.estado)
        print("")
    def eliminaralumno(self, id):
        session.query(Alumno).filter(Alumno.id == id).delete()
        session.query(Notas).filter(Notas.id == id).delete()


        session.commit()








