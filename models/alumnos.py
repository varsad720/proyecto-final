from sqlalchemy import Column, Float, String, Integer

from db.connector import *

class Alumno(Base):

    __tablename__ = 'alumnos'
    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(20))
    apellido = Column(String(20))
    nivel = Column(String(20))
    salon = Column(String(20))


class Notas(Base):

    __tablename__ = 'notas'
    id = Column(Integer, primary_key=True)
    nota1 = Column(Float(3))
    nota2 = Column(Float(3))
    nota3 = Column(Float(3))
    promedio = Column(Float(3))
    estado = Column(String(10))





