from db.connector import *
from db.DML import DML


if __name__ == '__main__':

    opc = 0
    salir = 5

    while opc != salir:

        print("=======>MENÚ DE NOTAS FINALES<=======")
        print("\n\t\t(1)CALIFICAR ALUMNOS"
              "\n\t\t(2)LISTA DE ALUMNOS"
              "\n\t\t(3)LISTA DE NOTAS"
              "\n\t\t(4)EDITAR ALUMNO"
              "\n\t\t(5)ELIMINAR ALUMNO"
              "\n\t\t(6)SALIR\n")
        opc = int(input("===Ingrese un número: "))

        DML1 = DML()
        if opc == 1:
            DML1.calificaralumno()
            input("Agregado a la base de datos con éxito"
                  "\nPRESIONE ENTER PARA VOLVER")

        if opc == 2:
            print("\t-----LISTA DE ALUMNOS-----\n")
            DML1.listaralumnos()

        if opc == 3:
            print("\t-----NOTAS DE ALUMNOS-----\n")
            DML1.listarnotas()

        if opc == 4:
            print("\t-----EDITAR ALUMNO-----\n")


            DML1.editaralumno()
            print("DATOS MODIFICADOS CON ÉXITO.")
            input("PRESIONE ENTER PARA VOLVER")

        if opc == 5:
            nid = input("Introduzca el id del alumno que desea eliminar: ")
            DML1.eliminaralumno(nid)
            print("ALUMNO Y NOTAS ELIMINADOS.")
            input("PRESIONE ENTER PARA VOLVER")

        if opc == 6:
            break